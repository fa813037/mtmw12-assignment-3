# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 15:02:45 2021

@author: ASUS
"""

from function import *
import numpy as np
import matplotlib.pyplot as plt

def geostrophicWind():
    '''Calulate the geostrophic with analytically and numerically and plot'''
    N = 10                     #the number of intervals to divide space into
    ymin = physProps['ymin']
    ymax = physProps['ymax']
    print(ymin)
    dy = (ymax - ymin)/N
    
    # The spatial dimension , y:
    y=np.linspace(ymin , ymax , N+1)
    
    #The pressure at the y points and the exact geosrophic wind
    p=pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    #The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy , physProps)
    
    #The pressure gradient and wind using five point differences
    dpdy = gradient_5point(p, dy)
    u_5point = geoWind(dpdy , physProps)
    
    #calculate the error value of two methods
    error = uExact - u_2point
    error2 = uExact - u_5point
    print(error)
    print(error2)
    
    #graph to compare the numerical and analytic solutions
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label ='Exact')
    plt.plot(y/1000, u_2point, '*k--', label ='Two-point differences',\
              ms = 12, markeredgewidth = 1.5)
    plt.plot(y/1000, u_2point, label ='five-point differences',\
              ms = 12, markeredgewidth = 1.5)
        
    plt.legend(loc = 'best')
    plt.xlabel('y(km)')
    plt.ylabel('u(m/s)')
    plt.tight_layout()
    plt.show()
    plt.plot(y/1000, error, 'k-', label ='error')
    plt.plot(y/1000, error2, 'k--', label ='error2')    
    plt.legend(loc = 'best')
    plt.xlabel('y(km)')
    plt.ylabel('u(m/s)')
    plt.show()
    
    
geostrophicWind()
