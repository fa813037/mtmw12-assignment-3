# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 14:59:30 2021

@author: ASUS
"""

import numpy as np
import matplotlib.pyplot as plt

# Physical parameters of the problem

physProps = {'pa' : 1e5,        # the mean pressure
             'pb' : 200.,       # the magnitude of the pressure variations
             'f'  : 1e-4,       # the Coriolis parameter
             'rho': 1.,         # density
             'L'  : 2.4e6,      # length scale of the pressure variations(k)
             'ymin': 0.,        # Start of the y domain
             'ymax': 1e6}       # End of the y domain


def pressure(y,props):
    '''The pressure and given y locations based on dictionary of physical 
    properties , props '''
    pa = props['pa']            # 
    pb = props['pb']
    L = props['L']
    return pa + pb*np.cos(y*np.pi/L)

def uGeoExact(y , props):
    '''The analytic geostrophic wind at given locations , y based on dictionary
    of physical properties , props '''
    pb = props['pb']
    L = props['L']
    rho = props['rho']
    f = props['f']
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)
        
def geoWind(dpdy , props):
    '''The geostrophic wind as a function of pressure gradient based on
    dictionary of physical properties , props '''
    rho = props['rho']
    f = props['f']
    return -dpdy/(rho*f)

# Funtions for calculating gradients
def gradient_2point(f, dx):
    '''The gradient of one dimensional array f assuming points are a distance
    dx apart using 2-point differences. Returns an array the sanme size as f'''
    
    dfdx = np.zeros_like(f)
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx  
    for i in range(1, len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1])/(2*dx)
    return dfdx

def gradient_5point(f, dx):
    dfdx = np.zeros_like(f)
    dfdx[1] = (f[2]-f[1])/dx
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx
    dfdx[-2] = (f[-2]-f[-3])/dx
    for i in range(2, len(f)-2):
        dfdx[i] = (-f[i+2] + 8*f[i+1] - 8*f[i-1] + f[i-2])/(12*dx) 
    return dfdx